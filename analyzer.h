#ifndef ANALYER_H
#define ANALYER_H

#include "raw.h"

#define GROUP_SIZE 6

class RawGroup {
private:
  uint16_t avg;
  uint16_t max;
  uint16_t min;
  uint16_t cnt;

public:
  RawGroup();

  int  AddValue(uint16_t value);
  void Dump();
  void Clean();
};

class RawDistribution {
private:
  RawGroup rawGroup[GROUP_SIZE];

public:
  int  AddValue(uint16_t value);
  void Dump();
  void Clean();
};

class Analyzer {
private:
  RawDistribution markDistribution;
  RawDistribution spaceDistribution;

public:
  void Analyze(raw_t *raw);
  void Dump();
  void Clean();
};

#endif
