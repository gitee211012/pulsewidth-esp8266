// Copyright 2021 WU PENG. All rights reserved.
//
#ifndef RECORDER_H
#define RECORDER_H

#include "Arduino.h"
#include "raw.h"

#define RECORDER_TIMEOVER         500000 // 0.5s

void recorder_init(int pin);
void recorder_start();
int  recorder_available();
void recorder_read(raw_t *raw);
void recorder_dump(raw_t *raw);

#endif
