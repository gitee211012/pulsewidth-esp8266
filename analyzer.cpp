#include "analyzer.h"

/*
GREE YAPOF
*/

int isInRange(uint16_t value, uint16_t min, uint16_t max) {
  return (value >= min && value <= max) ? 1 : 0;
}

RawGroup::RawGroup() {
  avg = 0;
  max = 0;
  min = 0;
  cnt = 0;
}

int RawGroup::AddValue(uint16_t value) {
  if (cnt == 0) {
    avg = value;
    max = value;
    min = value;
    cnt = 1;
    return 1;
  }
  
  if (value < (avg - avg/8) || value > (avg + avg/8)) { // (-12.5%, +12.5%)
    return 0;
  }

  uint32_t t = avg;
  t *= cnt;
  t += value;
  t /= (cnt + 1);
  avg = t;

  if (value > max) {
    max = value;
  }

  if (value < min) {
    min = value;
  }

  cnt++;

  return 1;
}

void RawGroup::Dump() {
  Serial.printf("  avg: %5d,   max: %5d,   min: %5d,   cnt: %3d\n", avg, max, min, cnt);
}

void RawGroup::Clean() {
  avg = 0;
  max = 0;
  min = 0;
  cnt = 0;  
}

int RawDistribution::AddValue(uint16_t value) {
  for (int i=0;i<GROUP_SIZE;i++) {
    if (rawGroup[i].AddValue(value)) {
      return 1;
    }
  }
  return 0;
}

void RawDistribution::Dump() {
  for (int i=0;i<GROUP_SIZE;i++) {
    rawGroup[i].Dump();
  }
}

void RawDistribution::Clean() {
  for (int i=0;i<GROUP_SIZE;i++) {
    rawGroup[i].Clean();
  }  
}

void Analyzer::Analyze(raw_t *raw) {
  Clean();
  for (int i=0;i<raw->length;i++) {
    if (i%2==0) {
      markDistribution.AddValue(raw->buffer[i]);
    } else {
      spaceDistribution.AddValue(raw->buffer[i]);
    }
  }
}

void Analyzer::Dump() {
  Serial.println("Mark Distribution");
  markDistribution.Dump();
  Serial.println("Space Distribution");
  spaceDistribution.Dump();
}

void Analyzer::Clean() {
  markDistribution.Clean();
  spaceDistribution.Clean();
}
