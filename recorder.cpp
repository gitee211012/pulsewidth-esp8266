// Copyright 2021 WU PENG. All rights reserved.
//
#include "recorder.h"

const uint8_t idleState  = 0;
const uint8_t busyState  = 1;
const uint8_t overState  = 2;

volatile static raw_t _raw;

volatile static uint32_t start1 = 0;
volatile static uint32_t start2 = 0;
volatile static uint8_t  state  = 0;

static void ICACHE_RAM_ATTR gpio_intr() {
  uint32_t now = micros();

  switch (state) {
  case idleState:
    start1 = now;
    start2 = now;
    state = busyState;
    break;
  case busyState:
    if (_raw.length < RECORDER_RAW_BUFFER_SIZE) {
      _raw.buffer[_raw.length++] = now - start2;
    } else {
      state = overState;
    }
    break;
  case overState:
    return;
    break;
  }
  start2 = now;
}

void recorder_init(int pin) {
  pinMode(pin, INPUT);
  attachInterrupt(pin, gpio_intr, CHANGE);
}

void recorder_start() {
  for (int i=0;i<RECORDER_RAW_BUFFER_SIZE;i++) {
    _raw.buffer[i] = 0;
  }
  _raw.length = 0;
  state = idleState;
}

int recorder_available() {
  if (state == idleState) {
    return 0;
  }
  
  if (state == busyState) {
    if (micros() - start1 < RECORDER_TIMEOVER) {
      return 0;
    } else {
      state = overState;
    }
  }

  return _raw.length;
}

void recorder_read(raw_t *raw) {
  for (uint16_t i=0;i<RECORDER_RAW_BUFFER_SIZE;i++) {
    raw->buffer[i] = _raw.buffer[i];
  }
  raw->length = _raw.length;

  _raw.length = 0;
  state = idleState;
}

void recorder_dump(raw_t *raw) {
  Serial.printf("\n");
  for (int i=0;i<raw->length;i+=2) {
    Serial.printf("{%5u, %5u}, ", raw->buffer[i], raw->buffer[i+1]);
    if (i % 8 == 6) {
      Serial.printf("\n");
    }
  }
  if (raw->length % 8 != 6) {
    Serial.printf("\n");
  }
  Serial.printf("Count: %d Raws\n", raw->length);
  Serial.printf("\n");
}
