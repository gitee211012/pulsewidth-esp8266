// Copyright 2021 WU PENG. All rights reserved.
//
#include "sender.h"

static void _delayMicroseconds(uint32_t us) {
  if (us <= 10000) {
    delayMicroseconds(us);
  } else {
    delay(us/1000UL);
    delayMicroseconds(us % 1000);
  }
}

Sender::Sender(int pin, uint32_t freq, uint8_t duty) {
  this->pin       = pin;
  this->freq      = freq;
  this->duty      = duty;
  this->period    = 1000000 / freq;
  this->onPeriod  = period * duty / 100;
  this->offPeriod = period - onPeriod;  
}

Sender::~Sender() {
  
}

void Sender::begin() {
  pinMode(this->pin, OUTPUT);
  digitalWrite(this->pin, LOW);
}

void Sender::ledOn() {
  digitalWrite(this->pin, HIGH);
}

void Sender::ledOff() {
  digitalWrite(this->pin, LOW);
}

void Sender::mark(uint16_t width) {
  uint32_t start, elapsed;

  start = micros();
  elapsed = micros() - start;
  while (elapsed < width) {
    this->ledOn();
    if (elapsed + this->onPeriod >= width) {
      _delayMicroseconds(width - elapsed);
      this->ledOff();
      return;
    }
    _delayMicroseconds(this->onPeriod);

    this->ledOff();
    if (elapsed + this->onPeriod + this->offPeriod >= width) {
      _delayMicroseconds(width - elapsed - this->onPeriod);
      return;
    }
    _delayMicroseconds(this->offPeriod);

    elapsed = micros() - start;
  }
}

void Sender::space(uint32_t width) {
  ledOff();
  _delayMicroseconds(width);
}

void Sender::send(raw_t *raw) {
  for (int i=0;i<raw->length;i++) {
    if (i%2==0) {
      this->mark(raw->buffer[i]);
    } else {
      this->space(raw->buffer[i]);
    }
  }
  if (raw->length%2==1) {
    this->space(40000);
  }
}
