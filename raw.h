#ifndef RAW_H
#define RAW_H

#include "Arduino.h"

#define RECORDER_RAW_BUFFER_SIZE 300

typedef struct {
  uint16_t buffer[RECORDER_RAW_BUFFER_SIZE];
  uint16_t length;
}raw_t;

#endif
