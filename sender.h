// Copyright 2021 WU PENG. All rights reserved.
//
#ifndef SENDER_H
#define SENDER_H

#include "raw.h"

class Sender {
private:
  int      pin;
  uint32_t freq;
  uint8_t  duty;
  uint32_t period;
  uint16_t onPeriod;
  uint16_t offPeriod;

  void ledOn();
  void ledOff();
  void mark(uint16_t width);
  void space(uint32_t width);

public:
  Sender(int pin, uint32_t freq = 38000, uint8_t duty = 25);
  ~Sender();

  void begin();
  void send(raw_t *raw);
};

#endif
